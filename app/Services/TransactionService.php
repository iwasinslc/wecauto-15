<?php


namespace App\Services;

use App\Models\User;
use Illuminate\Support\Str;

class TransactionService
{
    const TYPE_TRANSFER = 'transfer';
    const TYPE_WITHDRAW = 'withdraw';

    // Confirmation code expire in 10 minutes
    const CONFIRMATION_CODE_EXPIRE_IN = 10;

    /**
     * @return string
     */
    public function getConfirmationCode() {
        return Str::random(64);
    }

    /**
     * @param $user User
     * @param $data
     * @throws
     */
    public function sendConfirmation($user, $data) {
        $status = $user->sendEmailNotification('transaction_confirmation', $data);
        if($status === false) throw new \Exception('Email can not be sent');
    }

    /**
     * Check if amount of balance is enough for transaction
     * @param $transaction
     * @param $wallet
     * @throws \Exception
     */
    public function checkBalance($transaction, $wallet) {
        if($transaction->amount > $wallet->balance) {
            throw new \Exception(__('Not enough funds on wallet!'));
        }
    }

    /**
     * Check transaction expiration
     * @param $transaction
     * @throws \Exception
     */
    public function checkTransactionExpiration($transaction) {
        if($transaction->confirmation_sent_at->diffInMinutes(now()) > self::CONFIRMATION_CODE_EXPIRE_IN) {
            throw new \Exception('Confirmation link expired!');
        }
    }
}