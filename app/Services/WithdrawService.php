<?php


namespace App\Services;


use App\Events\NotificationEvent;
use App\Models\Currency;
use App\Models\PaymentSystem;
use App\Models\Setting;
use App\Models\Transaction;
use App\Models\TransactionStatus;
use App\Models\TransactionType;
use App\Models\User;
use App\Models\Wallet;
use App\Models\Withdraw;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class WithdrawService extends TransactionService
{
    /**
     * @param User $user,
     * @param Wallet $wallet
     * @param float $amount
     * @param PaymentSystem $paymentSystem
     * @param Currency $currency
     * @param $external
     * @param $with_confirmation
     * @throws
     */
    public function create(
        User $user,
        Wallet $wallet,
        float $amount,
        PaymentSystem $paymentSystem,
        Currency $currency,
        $external,
        $with_confirmation = true
    ) {
        $amount = (float)abs($amount);

        if (!isset($user, $currency, $paymentSystem)) {
            throw new \Exception(__('Required parameters are skipped'));
        }

        if ($wallet->balance < $amount) {
            throw new \Exception(__('Requested amount including withdrawal commission exceeds the wallet balance'));
        }

        /** @var Wallet $needed_wallet */
        $psMinimumWithdrawArray = @json_decode($paymentSystem->minimum_withdraw, true);
        $psMinimumWithdraw = isset($psMinimumWithdrawArray[$currency->code])
            ? $psMinimumWithdrawArray[$currency->code]
            : 0;

        if ($amount < $psMinimumWithdraw) {
            throw new \Exception(__('Minimum withdraw amount is ') . $psMinimumWithdraw );
        }

        $commission           = (float)Setting::getValue('commission_'.strtolower($currency->code));
        $commission = ($commission*$amount*rate($wallet->currency->code, $currency->code))/100;
        $amountWithCommission = $amount*rate($wallet->currency->code, $currency->code);

        if ($wallet->currency->code=='WEC')
        {
            $commission = $commission*$amount*0.01;
            $amountWithCommission = $amount;
        }

        if($with_confirmation) {
            $confirmation_code = $this->getConfirmationCode();
            $confirmation_sent_at = now();
        }

        DB::beginTransaction();
        try {
            /** @var Withdraw $transaction */
            $transaction = Withdraw::create([
                'commission' => $commission,
                'user_id' => $user->id,
                'currency_id' => $currency->id,
                'wallet_id' => $wallet->id,
                'payment_system_id' => $paymentSystem->id,
                'amount' => $amountWithCommission,
                'status_id' => TransactionStatus::STATUS_CREATED,
                'source'=>$external,
                'confirmation_code' => $confirmation_code ?? null,
                'confirmation_sent_at' => $confirmation_sent_at ?? null
            ]);

            if($with_confirmation) {
                $this->sendConfirmation($user, [
                    'subject' => 'Withdraw confirmation',
                    'type' => self::TYPE_WITHDRAW,
                    'confirmation_code' => $confirmation_code
                ]);
            } else { // Confirm transfer without confirmation by email
                $this->confirm($transaction, $with_confirmation);
            }
            DB::commit();
        } catch (\Throwable $e) {
            DB::rollBack();
            throw $e;
        }
    }

    /**
     * @param Withdraw $transaction
     * @param boolean $with_confirmation
     * @throws \Throwable
     */
    public function confirm(Withdraw $transaction, $with_confirmation = false) {
        if($with_confirmation) { // Check expiration only if was send confirmation
            $this->checkTransactionExpiration($transaction);
        }
        DB::beginTransaction();
        try {
            $wallet = $transaction->wallet()->lockForUpdate()->first();
            $this->checkBalance($transaction, $wallet);
            $wallet->update([
                'balance' => $wallet->balance -  $transaction->amount*rate($transaction->currency->code, $wallet->currency->code)
            ]);
            $transaction->status_id = TransactionStatus::STATUS_CONFIRMED_BY_EMAIL;
            $transaction->save();
            // Send notification
            NotificationEvent::dispatch($transaction->user, 'notifications.withdraw_confirmed', []);
            // Commit changes
            DB::commit();
        } catch (\Throwable $e) {
            DB::rollBack();
            throw $e;
        }
    }

    /**
     * @param $confirmation_code
     * @return array|string|null
     * @throws \Exception
     */
    public function confirmByCode($confirmation_code) {
        $transaction = Withdraw::where('confirmation_code', $confirmation_code)
            ->where('status_id', TransactionStatus::STATUS_CREATED)
            ->first();
        if(!$transaction) {
            throw new \Exception('Withdraw not found');
        }
        $this->confirm($transaction, true);
        return __('Withdraw confirmed successfully');
    }

    /**
     * Approve withdraw
     * @param Withdraw $transaction
     * @throws \Exception
     */
    public function approve($transaction) {

        if ($transaction->isApproved()) {
            throw new \Exception(__('This request already processed.'));
        }

        /** @var Wallet $wallet */
        $wallet         = $transaction->wallet()->first();
        /** @var User $user */
        $user           = $wallet->user()->first();
        /** @var PaymentSystem $paymentSystem */
        $paymentSystem  = $transaction->paymentSystem()->first();
        /** @var Currency $currency */
        $currency       = $wallet->currency()->first();

        if(!isset($wallet, $user, $paymentSystem, $currency)) {
            throw new \Exception('Wallet, user, payment system or currency is not found for withdrawal approve.');
        }

        $ps = $paymentSystem->getClassInstance();

//        if (empty($wallet->external)) {
//            $msg = 'ERROR: wallet is empty';
//            $transaction->result = $msg;
//            $transaction->save();
//            throw new \Exception($msg);
//        }

        try {
            $batchId = $ps->transfer($transaction);
        } catch (\Exception $e) {
            $msg = 'ERROR: ' . $e->getMessage();

            $transaction->status_id = TransactionStatus::STATUS_ERROR;
            $transaction->result = $msg;
            $transaction->save();
            Log::error($msg, $e->getTrace());
            throw new \Exception($msg);
        }

        if (empty($batchId)) {
            $batchErr = __('Unable to approve request, payment system transfer is failed ..');

            $transaction->result = $batchErr;
            $transaction->status_id = TransactionStatus::STATUS_ERROR;
            $transaction->save();
            Log::error($batchErr);
            throw new \Exception($batchErr);
        }

        $transaction->update([
            'batch_id' => $batchId,
            'status_id' => TransactionStatus::STATUS_APPROVED,
        ]);

//        $data = [
//            'withdraw_amount' => $transaction->amount,
//            'currency'        => $currency,
//            'payment_system'  => $paymentSystem
//        ];
//        $user->sendNotification('approved_withdrawal', $data);

        NotificationEvent::dispatch($user, 'notifications.approved_withdrawal', [
            //'id'=>$user->id,
            'user_id'=>$user->id,
            'amount'=>$transaction->amount,
            'currency'=>$currency->code,

        ]);

        try {
            $ps->getBalances();
        } catch (\Exception $e) {
            Log::error('ERROR: ' . $e->getMessage(), $e->getTrace());
            throw new \Exception('ERROR: ' . $e->getMessage());
        }
    }

    /**
     * Reject withdraw
     * @param $transaction_id
     * @param User $owner // Show that owner must be checked
     * @throws \Exception
     */
    public function reject($transaction_id, $owner = null) {
        /**
         * @var $transaction Withdraw
         */
        if($owner) {
            $transaction = $owner->withdraws()->find($transaction_id);
        } else {
            $transaction = Withdraw::find($transaction_id);
        }

        if(empty($transaction)) {
            throw new \Exception(__('Withdraw not found'));
        }

        if ($transaction->isApproved() || $transaction->isRejected()) {
            throw new \Exception(__('This request already processed.'));
        }

        /** @var Wallet $wallet */
        $wallet         = $transaction->wallet()->first();
        /** @var User $user */
        $user           = $owner ?? $wallet->user()->first();
        /** @var PaymentSystem $paymentSystem */
        $paymentSystem  = $transaction->paymentSystem()->first();
        /** @var Currency $currency */
        $currency       = $transaction->currency()->first();
        $amount         = $transaction->amount;

        if (!isset($wallet, $user, $paymentSystem, $currency)) {
            throw new \Exception(__('Wallet, user, payment system or currency is not found for withdrawal reject.'));
        }

        $wallet->returnFromRejectedWithdrawal($transaction);

        if ($currency->code=='ACC')
        {
            $user->addLimitsSell($amount, $currency->code, $transaction->rate);
        }

        $transaction->update([
            'status_id' => TransactionStatus::STATUS_REJECTED
        ]);

        NotificationEvent::dispatch($user, 'notifications.rejected_withdrawal', [
            //'id'=>$user->id,
            'user_id'=>$user->id,
            'amount'=>$amount,
            'currency'=>$currency->code,
        ]);
    }

    /**
     * Create swap withdraw without confirmation
     * @param Wallet $wallet
     * @param float $amount
     * @return |null
     */
    public function createWithdrawSwap(Wallet $wallet, float $amount) {
        $amount         = (float) abs($amount);
        /** @var TransactionType $type */
        /** @var User $user */
        $user           = $wallet->user()->first();
        /** @var Currency $currency */
        $currency       = $wallet->currency()->first();
        /** @var PaymentSystem $paymentSystem */
        $paymentSystem  = $wallet->paymentSystem()->first();

        if (!isset($user, $currency, $paymentSystem)) {
            return null;
        }

        $amountWithCommission = $amount;

        /** @var Transaction $transaction */
        $transaction = Withdraw::create([
            'commission'        => 0,
            'user_id'           => $user->id,
            'status_id'         => TransactionStatus::STATUS_CREATED,
            'currency_id'       => $currency->id,
            'wallet_id'         => $wallet->id,
            'payment_system_id' => $paymentSystem->id,
            'amount'            => $amountWithCommission,
            'result'            => $wallet->balance
        ]);

        $wallet->update([
            'balance' => $wallet->balance - $amountWithCommission
        ]);

        $user->removeLimitsSell($amount, $currency->code);

        return boolval($transaction);
    }

}