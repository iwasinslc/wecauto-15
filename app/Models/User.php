<?php

namespace App\Models;

use App\Console\Commands\Automatic\ScriptCheckerCommand;
use App\Events\NotificationEvent;
use App\Http\Controllers\Auth\LoginController;
use App\Jobs\HandleRankJob;
use App\Jobs\TelegramNotificationJob;
use App\Mail\NotificationMail;
use App\Models\Telegram\TelegramBots;
use App\Models\Telegram\TelegramUsers;
use App\Models\UserTasks\UserTaskActions;
use App\Models\UserTasks\UserTaskPropositions;
use App\Models\UserTasks\UserTasks;
use App\Models\Youtube\YoutubeVideoWatch;
use App\Traits\ModelTrait;
use App\Traits\Uuids;
use Carbon\Carbon;

use Illuminate\Contracts\Cache\Repository;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Lab404\Impersonate\Models\Impersonate;
use Spatie\Permission\Traits\HasRoles;
use Webpatser\Uuid\Uuid;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;


/**
 * Class User
 * @package App\Models
 *
 * @property string id
 * @property string name
 * @property string email
 * @property string login
 * @property string password
 * @property string phone
 * @property string partner_id
 * @property string my_id
 * @property string remember_token
 * @property bool tiketit_admin
 * @property bool tiketit_agent
 * @property string blockio_wallet_btc
 * @property string blockio_wallet_ltc
 * @property string blockio_wallet_doge
 * @property string sex
 * @property string country
 * @property string city
 * @property float latitude
 * @property float longitude
 * @property Carbon created_at
 * @property Carbon updated_at
 * @property Carbon email_verified_at
 * @property Carbon email_verification_sent
 * @property string email_verification_hash
 * @property integer left_line
 * @property integer google_2fa
 * @property integer google_2fa_code
 * @property string licence_id
 * @property Carbon close_at
 * @property string locale
 * @property Licences licence
 * @property string avatar
 * @property integer rank_id
 * @property UserRank rank
 * @property float sell_limit
 * @property float buy_limit
 * @property string partner_text
 * @property string cyber_id
 *
 * @property integer representative
 * @property float refback
 *
 * @property Carbon id_bought_datetime
 */
class User extends Authenticatable implements HasMedia
{
    use ModelTrait;
    use Notifiable;
    use HasRoles;
    use Uuids;
    use Impersonate;
    use InteractsWithMedia;

    const MAX_LOGIN_ATTEMPTS = 5;
    const LOGIN_BLOCKING = 60;
    const LIMIT_ALLOWED_TRANSFERS_IN_HOURS = 36;

    /** @var bool $incrementing */
    public $incrementing = false;
    protected $keyType = 'string';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'email',
        'login',
        'password',
        'my_id',
        'partner_id',
        'phone',
        'skype',
        'created_at',
        'blockio_wallet_btc',
        'blockio_wallet_ltc',
        'blockio_wallet_doge',
        'sex',
        'city',
        'country',
        'longitude',
        'latitude',
        'email_verified_at',
        'email_verification_sent',
        'email_verification_hash',
        'sell_limit',
        'buy_limit',
        'left_line',
        'representative',
        'refback',
        'id_bought_datetime',
        'google_2fa',
        'google_2fa_code',
        'licence_id',
        'close_at',
        'avatar',
        'locale',
        'rank_id',
        'partner_text',
        'cyber_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token'
    ];

    protected $dates = [
        'close_at',
        'email_verified_at',
        'email_verification_sent'
    ];

    public function licence()
    {
        return $this->belongsTo(Licences::class, 'licence_id');
    }


    public function activeLicence()
    {
        if ($this->close_at === null) {
            return false;
        }
        return now() < $this->close_at;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function transactions()
    {
        return $this->hasMany(Transaction::class, 'user_id');
    }

    /**
     * All user's withdraws
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function withdraws()
    {
        return $this->hasMany(Withdraw::class, 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function cashback()
    {
        return $this->hasMany(CashbackRequest::class, 'user_id');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function notifications()
    {
        return $this->hasMany(Notification::class, 'user_id')->orderBy('notifications.created_at', 'desc');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\hasOne
     */
    public function partner()
    {
        return $this->hasOne(User::class, 'my_id', 'partner_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function rank()
    {
        return $this->hasOne(UserRank::class, 'id', 'rank_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function orderPieces()
    {
        return $this->hasMany(OrderPiece::class, 'user_id');
    }

    /**
     * @return bool
     */
    public function canBuyLicence()
    {
        if ($this->checkDepositLimit())
        {
            return true;
        }

        if ($this->sell_limit<=1)
        {
            return true;
        }


        if ($this->buy_limit<=1)
        {
            return true;
        }

        return false;

    }

    /**
     * @return bool
     */

    public function checkDepositLimit()
    {
        if (!$this->activeLicence())
        {
            return true;
        }

//        $check_dep = cache()->tags('userDepositLimit.' . $this->id)->remember('c.' . $this->id . '.userDepositLimit',now()->addDay(),  function () {
            return $this->deposits()->where('created_at', '>', $this->close_at->subDays($this->licence->duration)->toDateTimeString())->exists();
//        });
//
//        return $check_dep;
    }

    /**
     * @return float|int|mixed
     */
    public function dailyLimit()
    {


        if ($this->licence===null||!$this->activeLicence())
        {
            return 0;
        }

        $percent = $this->rank->daily_limit;
        if (now()->toDateTimeString()<='2020-12-31 15:00:00'&&now()->toDateTimeString()>='2020-12-21 15:00:00')
        {
            $percent = 10;
        }

        $currency = Currency::getByCode('FST');

        $limit = $this->sellLimit() * $percent * 0.01;


        $start = now()->startOfDay()->toDateTimeString();

        $parse = $this->close_at->subDays($this->licence->duration)->toDateTimeString();

        if ($parse>$start)
        {
            $start = $parse;
        }

        $orders = $this->exchangeOrders()
            ->active()
            ->where('type', ExchangeOrder::TYPE_SELL)
            ->where('main_currency_id', $currency->id)
            ->whereBetween('exchange_orders.created_at', [$start, now()->toDateTimeString()])
            ->get();

        $orders_sum = 0;
        /**
         * @var ExchangeOrder $order
         */
        foreach ($orders as $order) {
            $orders_sum = $orders_sum + ($order->amount * $order->licence_rate);
        }


        $pieces = $this->orderPieces()->where('type', ExchangeOrder::TYPE_SELL)
            ->where('main_currency_id', $currency->id)
            ->whereHas('order', function ($query)  {
                $query->where('user_id','!=', $this->id);
            })
            ->whereBetween('order_pieces.created_at', [$start, now()->toDateTimeString()])->sum('limit_amount');


        $pieces_order = $this->orderPieces()->where('type', ExchangeOrder::TYPE_SELL)
            ->where('main_currency_id', $currency->id)
            ->whereHas('order', function ($query) use ($start) {
                $query->where('user_id', $this->id)
                    ->whereBetween('exchange_orders.created_at', [$start, now()->toDateTimeString()]);
            })
            ->sum('limit_amount');

        $sum = $orders_sum + $pieces+$pieces_order;


        $return =  $limit - $sum;

        return $return>0 ? $return : 0;


    }

    /**
     * @param $amount
     * @param $currency
     * @param int $rate
     * @return bool
     */
    public function removeLimitsSell($amount, $currency, $rate = 0)
    {
        $this->refresh();
        if ($rate == 0) $rate = rate($currency, 'USD');

        $amount = $amount * $rate;


        if ($this->licence === null) {
            return false;
        }

        if ($this->sell_limit < $amount) {
            $this->sell_limit = 0;
        } else {
            $this->sell_limit = $this->sell_limit - $amount;
        }
        return $this->save();
    }

    /**
     * @param $amount
     * @param $currency
     * @param int $rate
     * @return bool
     */
    public function removeLimitsBuy($amount, $currency, $rate = 0)
    {
        $this->refresh();
        if ($rate == 0) $rate = rate($currency, 'USD');

        $amount = $amount * $rate;


        if ($this->licence === null) {
            return false;
        }


        if ($this->buy_limit < $amount) {
            $this->buy_limit = 0;
        } else {
            $this->buy_limit = $this->buy_limit - $amount;
        }
        return $this->save();
    }

    /**
     * @param $amount
     * @param $currency
     * @param int $rate
     * @return bool
     */
    public function addLimitsSell($amount, $currency, $rate = 0)
    {

        if ($this->licence === null) {
            return false;
        }

        $this->refresh();

        if ($rate == 0) $rate = rate($currency, 'USD');

        $amount = $amount * $rate;


        $this->sell_limit = $this->sell_limit + $amount;


        if ($this->sell_limit > $this->licence->sell_amount) {
            $this->sell_limit = $this->licence->sell_amount;
        }

        return $this->save();
    }

    /**
     * @param $amount
     * @param $currency
     * @param int $rate
     * @return bool
     */
    public function addLimitsBuy($amount, $currency, $rate = 0)
    {
        if ($this->licence === null) {
            return false;
        }


        $this->refresh();
        if ($rate == 0) $rate = rate($currency, 'USD');

        $amount = $amount * $rate;


        $this->buy_limit = $this->buy_limit + $amount;

        if ($this->buy_limit > $this->licence->buy_amount) {
            $this->buy_limit = $this->licence->buy_amount;
        }

        return $this->save();
    }

    /**
     * @param string $code
     * @return float|int
     */
    public function sellLimit($code = 'USD')
    {


        if (!$this->activeLicence()) {
            return 0;
        }


        if ($this->licence === null) {
            return 0;
        }


        if ($this->sell_limit === null) {
            return 0;

        }


        return $this->sell_limit > 0 ? $this->sell_limit : 0;


    }

    /**
     * @param string $code
     * @return float|int
     */
    public function buyLimit($code = 'USD')
    {
        if (!$this->activeLicence()) {
            return 0;
        }


        if ($this->licence === null) {
            return 0;
        }


        if ($this->buy_limit === null) {
            return 0;

        }


        return $this->buy_limit > 0 ? $this->buy_limit : 0;
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function exchangeOrders()
    {
        return $this->hasMany(ExchangeOrder::class, 'user_id');
    }

    /**
     * @param User $parent
     */
    public function generatePartnerTree(User $parent)
    {
        $parent_array = [];

        $partners = $parent->partners()->orderBy('pivot_line', 'asc')->wherePivot('line', '<', 10)->get();
        $parent_array[$parent->id] = ['line' => 1];
        $i = 1;

        NotificationEvent::dispatch($parent, 'notifications.new_partner', [
            //'id'=>$user->id,
            'user_id' => $parent->id,
            'login' => $this->login,
            'level' => $i,

        ]);

        foreach ($partners as $partner) {
            $i++;
            $parent_array[$partner->id] = ['line' => $i];

            if ($i <= 10) {
                NotificationEvent::dispatch($partner, 'notifications.new_partner', [
                    //'id'=>$user->id,
                    'user_id' => $partner->id,
                    'login' => $this->login,
                    'level' => $i,

                ]);
            }
        }
        $this->partners()->sync($parent_array);

    }


    /**
     * @param $code
     * @param null $ps
     * @return Wallet
     */
    public function getUserWallet($code, $ps = null)
    {
        $currency = \App\Models\Currency::getByCode($code);

        $wallet = cache()->tags('wallets_' . $this->id)
            ->rememberForever('wallet_' . $this->id . '_' . $currency->id . ($ps ? '_' . $ps : ''), function() use($currency, $ps) {
                $query = $wallet = \App\Models\Wallet::where('user_id', $this->id)
                    ->where('currency_id', $currency->id);

                if ($currency->code=='USD' && $ps===null)
                {
                    $ps = 'perfectmoney';
                }

                if ($ps !== null) {
                    $query = $query->where('payment_system_id', \App\Models\PaymentSystem::getByCode($ps)->id);
                }

                $wallet = $query->first();

                if ($wallet === null) {
                    $ps_obj = $ps === null ? \App\Models\PaymentSystem::whereHas('currencies', function ($query) use ($currency) {
                        $query->where('code', $currency->code);
                    })->first() : \App\Models\PaymentSystem::getByCode($ps);
                    $wallet = Wallet::newWallet($this, $currency, $ps_obj);
                }

                return $wallet;
            }
        );

        return $wallet;
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function userTaskActions()
    {
        return $this->hasMany(UserTaskActions::class, 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function userTasks()
    {
        return $this->hasMany(UserTasks::class, 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function wallets()
    {
        return $this->hasMany(Wallet::class, 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function telegramUser()
    {
        return $this->hasMany(TelegramUsers::class, 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function taskPropositions()
    {
        return $this->hasMany(UserTaskPropositions::class, 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function socialMeta()
    {
        return $this->hasMany(UsersSocialMeta::class, 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function youtubeVideoWatches()
    {
        return $this->hasMany(YoutubeVideoWatch::class, 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function deposits()
    {
        return $this->hasMany(Deposit::class, 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function user_ips()
    {
        return $this->hasMany(UserIp::class, 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function mailSents()
    {
        return $this->hasMany(MailSent::class, 'user_id');
    }

    /**
     * @param boolean $useSymbols
     * @param string $currencyId
     * @return array
     */
    public function getBalancesByCurrency($useSymbols = false, $currencyId = null): array
    {
        $wallets = $this->wallets()->with([
            'currency'
        ]);

        if (null !== $currencyId) {
            $wallets = $wallets->where('currency_id', $currencyId);
        }

        $wallets = $wallets->get();
        $balances = [];

        foreach ($wallets as $wallet) {
            $arrayKey = true === $useSymbols ? $wallet->currency->symbol : $wallet->currency->code;

            if (!isset($balances[$arrayKey])) {
                $balances[$arrayKey] = 0;
            }

            $balances[$arrayKey] += round($wallet->balance, $wallet->currency->precision);
        }

        return $balances;
    }

    /**
     * @param bool $useSymbols
     * @param string $operationType
     * @param int $approved
     * @return array
     * @throws \Exception
     */
    public function getTotalByTransactions($useSymbols = false, $operationType = null, $approved = 0): array
    {
        $total = [];
        $currencies = getCurrencies();

        foreach ($currencies as $currency) {
            if(isset($operationType) && $operationType == TransactionType::TRANSACTION_TYPE_WITHDRAW) {
                $amount = Withdraw::query();
                if (0 != $approved) {
                    $amount = $amount->where('withdraws.status_id', TransactionStatus::STATUS_APPROVED);
                }
            } else {
                $amount = Transaction::join('transaction_types', function ($join) {
                    $join->on('transactions.type_id', '=', 'transaction_types.id');
                });
                if (null !== $operationType) {
                    $amount = $amount->where('transaction_types.name', $operationType);
                }
                if (0 != $approved) {
                    $amount = $amount->where('transactions.approved', $approved);
                }
            }

            $amount = $amount->where('currency_id', $currency['id'])
                ->where('user_id', $this->id)
                ->sum('amount');
            $arrayKey = true === $useSymbols ? $currency['symbol'] : $currency['code'];

            if (!isset($total[$arrayKey])) {
                $total[$arrayKey] = 0;
            }

            $total[$arrayKey] += round($amount, $currency['precision']);
        }

        return $total;
    }

    /**
     * @return bool
     */
    public function hasReferrals()
    {
        return $this->referrals()->count() > 0;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function referrals()
    {
        return $this->belongsToMany(User::class, 'user_parents', 'parent_id', 'user_id')->withPivot(['line', 'main_parent_id']);
    }

    public function referralsDeposits()
    {
        return $this->belongsToMany(Deposit::class, 'user_parents', 'parent_id', 'user_id', null, 'user_id')->withPivot(['line', 'main_parent_id']);
//        return $this->hasMany(User::class, 'partner_id', 'my_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\belongsToMany
     */
    public function referralsTransactions()
    {
        return $this->belongsToMany(Transaction::class, 'user_parents', 'parent_id', 'user_id', null, 'user_id')->withPivot(['line', 'main_parent_id']);
//        return $this->hasMany(User::class, 'partner_id', 'my_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\belongsToMany
     */
    public function partners()
    {
        return $this->belongsToMany(User::class, 'user_parents', 'user_id', 'parent_id')->withPivot(['line', 'main_parent_id']);
//        return $this->hasMany(User::class, 'partner_id', 'my_id');
    }

    /**
     * @param int $level
     * @param int $json
     * @return mixed|null
     */
    public function getReferralsOnLevel($level = 1, $json = 0)
    {
        $all = $this->getAllReferrals();
        if (isset($all[$level])) {
            return $json === true ? json_encode($all[$level]) : $all[$level];
        }
        return null;
    }

    /**
     * @param int $level
     * @return array
     */
    public function getLevels()
    {
        $levels = [];
        $referrals = $this->referrals()->get()->groupBy('pivot.line', true);
        foreach ($referrals as $level => $allReferral) {
            $levels[$level] = $allReferral->count();
        }

        return $levels;
    }

    /**
     * @param int $level
     * @return mixed
     * @throws \Exception
     */
    public function getLevels24h()
    {

        $levels = [];
        $referrals = $this->referrals()
            ->where('created_at', '>', now()->subDay()->toDateTimeString())
            ->get()->groupBy('pivot.line', true);
        foreach ($referrals as $level => $allReferral) {
            $levels[$level] = $allReferral->count();
        }

        return $levels;
    }

    /**
     * @param integer $json
     * @param int $flag
     * @return array
     */
    public function getAllReferrals()
    {
        /** @var User $referrals */
        $referrals = $this->referrals()->get()->groupBy('pivot.line', true);

        return $referrals->toArray();
    }

    /**
     * @param $level
     * @return int
     */
    public function getReferralOnLoadPercent($level)
    {
        return Referral::getOnLoad($level);
    }

    /**
     * @param $level
     * @return int
     */
    public function getReferralOnProfitPercent($level)
    {
        return Referral::getOnProfit($level);
    }

    /**
     * @param $level
     * @return int
     */
    public function getReferralOnTaskPercent($level)
    {
        return Referral::getOnTask($level);
    }

    /**
     * @return array
     */
    public function getPartnerLevels()
    {

        $levels = $this->partners()->get()->pluck('pivot.line')->toArray();

        return !empty($levels) ? $levels : [];
    }

    /**
     * @param $plevel
     * @param bool $json
     * @return mixed
     */
    public function getPartnerOnLevel($plevel, bool $json = false)
    {

        $partner = $this->partners()->wherePivot('line', $plevel)->first();
        if (null != $partner) {
            if (true === $json) {
                return $partner->toArray();
            }
            return $partner;
        }


        return null;
    }

    /**
     * @param User $user |null
     * @param bool $json
     * @return array|string
     */
    public static function getReferralsTree(User $user = null, $json = false)
    {
        $user['referrals'] = [];

        if ($user->hasReferrals()) {
            foreach ($user->referrals()->get() as $referral) {
                $referral['deposits'] = $referral->deposits()
                    ->with('transactions')
                    ->get()
                    ->toArray();
                $referral['referrals'] = self::getReferralsTree($referral);

                if (false === $json) {
                    return $referral->toArray();
                }
                return $referral->toJson();
            }

            if (false === $json) {
                return $user->toArray();
            }
            return $user->toJson();
        }

        return null;
    }

    /**
     * @param User|null $user
     * @return array|Repository
     * @throws
     */
    public static function getD3V3ReferralsTree(User $user = null): array
    {
        if (empty($user)) {
            return [];
        }

        $referrals = [];
        $referrals['name'] = $user->email;

        if (!$user->hasReferrals()) {
            return $referrals;
        }

        foreach ($user->referrals()->get() as $r) {
            $referral = self::getD3V3ReferralsTree($r);
            $referrals['children'][] = $referral;
        }

        return $referrals;
    }


    /**
     * @return array
     * @throws \Exception
     */
    public static function accruedBalances(): array
    {
        $bonusBalances = Transaction::transactionBalances('bonus');
        $depositsBalances = Deposit::closedBalances();
        $referralRecharge = Transaction::transactionBalances('partner');
        $referralDeposit = Transaction::transactionBalances('dividend');

        foreach (Currency::all() as $currency) {
            $balances[$currency->code] = $depositsBalances[$currency->code] + $bonusBalances[$currency->code] + $referralDeposit[$currency->code] + $referralRecharge[$currency->code];
        }
        return isset($balances) ? $balances : [];
    }

    /**
     * @param string $code
     * @param array $data
     * @param int $delay
     * @throws \Throwable
     */
    public function sendNotification(string $code, array $data, int $delay = 0)
    {
        if (env('USER_NOTIFICATIONS_ENABLED', 1) == 1) {
            $this->sendEmailNotification($code, $data);
            //$this->sendTelegramNotification($code, $data, null, $delay);
        }
    }

    /**
     * @param string $code
     * @param array $data
     * @param bool $skipVerified
     * @param int $delay
     * @return bool
     * @throws \Throwable
     */
    public function sendEmailNotification(string $code, array $data, bool $skipVerified = false, int $delay = 0)
    {
        if (false === $skipVerified
            && config('mail.usersShouldVerifyEmail') == true
            && false === $this->isVerifiedEmail()) {
            \Log::info('User email is not verified for accepting mails.');
            return false;
        }

        $subjectView = 'mail.subject.' . $code;
        $bodyView = 'mail.body.' . $code;

        if (!view()->exists($subjectView) || !view()->exists($bodyView)) {
            return false;
        }

        $html = view('mail.body.' . $code, array_merge([
            'user' => $this,
        ], $data))->render();

        if (empty($html)) {
            return false;
        }

        $notificationMail = (new NotificationMail($this, $code, $data))
            ->onQueue(getSupervisorName() . '-emails')
            ->delay(now()->addSeconds($delay));
        \Mail::to($this)->queue($notificationMail);
    }

    /**
     * @param string $code
     * @param array $data
     * @param null $bot
     * @param int $delay
     */
    public function sendTelegramNotification(string $code, array $data, $bot = null, int $delay = 0)
    {
        TelegramNotificationJob::dispatch($this, $code, $data, $bot)->onQueue(getSupervisorName() . '-default')->delay(now()->addSeconds($delay));
    }

    /**
     * @param string $code
     * @param array $data
     * @throws \Throwable
     */
    public static function notifyAdmins(string $code, array $data)
    {
        self::notifyAdminsViaNotificationBot($code, $data);
        self::notifyAdminsViaEmail($code, $data);
    }

    /**
     * @param string $code
     * @param array $data
     * @throws \Throwable
     */
    public static function notifyAdminsViaEmail(string $code, array $data)
    {
        /*
         * Search admins
         */
        $adminRoles = \DB::table('roles')
            ->whereIn('name', ['root', 'admin'])
            ->get();
        $adminRolesIds = [];

        foreach ($adminRoles as $adminRole) {
            $adminRolesIds[] = $adminRole->id;
        }

        if (count($adminRolesIds) == 0) {
            return;
        }

        $roles = \DB::table('model_has_roles')
            ->whereIn('role_id', $adminRolesIds)
            ->where('model_type', 'App\Models\User')
            ->get();

        foreach ($roles as $role) {
            /** @var User $admin */
            $admin = User::find($role->model_id);
            \Log::info('Found admin for support email: ' . $admin->email);
            $admin->sendEmailNotification($code, $data, true);
        }
    }

    /**
     * @param string $code
     * @param array $data
     * @throws \Throwable
     */
    public static function notifyAdminsViaNotificationBot(string $code, array $data)
    {
        /*
         * Search admins
         */
        $adminRole = \DB::table('roles')
            ->where('name', 'root')
            ->first();

        if (null == $adminRole) {
            return;
        }

        $roles = \DB::table('model_has_roles')
            ->where('role_id', $adminRole->id)
            ->where('model_type', 'App\Models\User')
            ->get();
        /** @var TelegramBots $notificationBot */
        $notificationBot = TelegramBots::where('keyword', 'notification_bot')->first();

        foreach ($roles as $role) {
            /** @var User $admin */
            $admin = User::find($role->model_id);
            $admin->sendTelegramNotification($code, $data, $notificationBot);
        }
    }

    /**
     * @return void
     */
    public function addIp()
    {
        UserIp::addIp($this->id);
    }

    /**
     * @param string $password
     * @return User
     */
    public function setPassword(string $password): User
    {
        $this->password = Hash::make($password);
        $this->save();
        return $this;
    }

    /**
     * @return User|null
     */
    public static function topPartner()
    {
        $users = self::whereNotNull('partner_id')
            ->select(DB::raw('partner_id, count(1) as r_count'))
            ->groupBy('partner_id')
            ->get();

        if ($users->count() == 0) {
            return null;
        }

        $partner = $users->firstWhere('r_count', $users->max('r_count'));

        if (empty($partner)) {
            return null;
        }

        $user = self::where('my_id', $partner->partner_id)->first();

        if (empty($user)) {
            return null;
        }

        $user->referrals_amount = $partner->r_count;

        return $user;
    }

    /**
     * @return bool
     */
    public function isMan()
    {
        return $this->sex == 'man';
    }

    /**
     * @return bool
     */
    public function isWoman()
    {
        return $this->sex == 'woman';
    }

    /**
     * @param string $coordinates
     * @return bool
     */
    public function setCoordinatesLonLat(string $coordinates = '')
    {
        $coordinates = preg_replace('/ /', '', $coordinates);

        if (!preg_match('/\,/', $coordinates)) {
            return false;
        }

        $arrayCoordinates = explode(',', $coordinates); // longitude,latitude

        $this->longitude = $arrayCoordinates[0];
        $this->latitude = $arrayCoordinates[1];
        return $this->save();
    }

    /**
     * @param string $coordinates
     * @return bool
     */
    public function setCoordinatesLatLong(string $coordinates = '')
    {
        $coordinates = preg_replace('/ /', '', $coordinates);

        if (!preg_match('/\,/', $coordinates)) {
            return false;
        }

        $arrayCoordinates = explode(',', $coordinates); // latitude,longitude

        $this->latitude = $arrayCoordinates[0];
        $this->longitude = $arrayCoordinates[1];
        return $this->save();
    }

    /**
     * @return bool
     */
    public function isVerifiedEmail()
    {
        return $this->email_verified_at !== null;
    }

    /**
     * @return bool
     */
    public function canSendVerificationEmail()
    {
        if ($this->email_verification_sent == null) {
            return true;
        }

        return $this->email_verification_sent->diffInMinutes(now()) > 1;
    }

    /**
     * @return bool
     * @throws \Throwable
     */
    public function sendVerificationEmail()
    {
        if (config('mail.usersShouldVerifyEmail') != true) {
            return false;
        }

        if (false === $this->canSendVerificationEmail()) {
            return false;
        }

        if (empty($this->email)) {
            return false;
        }

        $this->email_verification_sent = now();
        $this->email_verification_hash = md5($this->email . config('app.name'));
        $this->save();

        $this->sendEmailNotification('email_verification', [
            'email_verification_hash' => $this->email_verification_hash,
        ], true);

        return true;
    }

    /**
     * @param string|null $email
     * @return string
     */
    private function getEmailVerificationHash(string $email = null)
    {
        if (null === $email) {
            return null;
        }

        return md5($email . config('app.name'));
    }

    /**
     * @return bool
     * @throws \Throwable
     */
    public function refreshEmailVerificationAndSendNew()
    {
        if ($this->email_verification_hash != $this->getEmailVerificationHash($this->email)) {
            return $this->sendVerificationEmail();
        }
    }

    /**
     * @param Media|null $media
     * @throws \Spatie\Image\Exceptions\InvalidManipulation
     */
    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('thumb')
            ->width(env('AVATAR_WIDTH', 145))
            ->height(env('AVATAR_HEIGHT', 145));
    }

    /**
     * @return bool
     */
    public function isRepresentative()
    {
        return $this->representative == 1;
    }

    /**
     * @return bool
     */
    public function isGoogle2FaEnabled()
    {
        return $this->google_2fa == 1;
    }

    /**
     * Google 2fa enable
     * @param $secret_code
     * @return $this
     */
    public function setGoogle2FaEnabled($secret_code)
    {
        $this->google_2fa = 1;
        $this->google_2fa_code = $secret_code;
        $this->save();
        return $this;
    }

    /**
     * Google 2fa disable
     * @return $this
     */
    public function setGoogle2FaDisabled()
    {
        $this->google_2fa = 0;
        $this->google_2fa_code = null;
        $this->save();
        return $this;
    }


    public function getAvatarPath()
    {
        if ($this->avatar == null) {
            return '/img/empty_user.png';
        }


        try {
            return Storage::disk('s3')->url($this->avatar);
        } catch (\Exception $e) {
            return '/img/empty_user.png';
        }

    }


    public function myReferrals()
    {
        return $this->hasMany(User::class, 'partner_id', 'my_id');
    }


    public function checkRank()
    {

        if (!$this->activeLicence()) return false;

        $licence = $this->licence;


        if ($licence===null) return false;

        $prev_rank_id = $this->rank_id;

        $ranks = UserRank::query()->orderBy('id', 'desc')->get();
        foreach ($ranks as $rank) {
            /**
             * @var UserRank $rank
             */
            $confines = json_decode($rank->confines, true);
            if (empty($confines))   continue;

            $rank_set = false;

            if ($licence->price < $confines['licence'])
            {
                //$rank_set = false;
                continue;
            }


            foreach ($confines['ranks'] as $rank_id => $count) {
                $my_refs = $this->myReferrals()->where('rank_id', '>=', $rank_id);
                if ($rank_id == 1) {
                    $my_refs = $my_refs->whereHas('deposits', function ($query) {
                        $query->where('active', true);
                    });
                }
                $refs_count = $my_refs->count();

                if ($refs_count >= $count)
                {

                    $rank_set = true;
                }

            }

            if ($rank_set===true)
            {
                $this->rank_id = $rank->id;
                $this->save();

                Transaction::set_rank($this->getUserWallet('WEC'), $this->rank_id);

                if ($rank_id>$prev_rank_id)
                {
                    $this->partner->checkRank();
                }

                //HandleRankJob::dispatch($this->partner)->onQueue(getSupervisorName() . '-high')->delay(now());

                break;
            }



        }
    }


//    public function checkRankTest()
//    {
//
//        if (!$this->activeLicence()) return false;
//
//        $licence = $this->licence;
//        if ($this->licence===null) return false;
//
//        $ranks = UserRank::where('id', '>', $this->rank_id)->orderBy('id', 'desc')->get();
//
//
//        foreach ($ranks as $rank) {
//
//            var_dump($rank->name);
//            /**
//             * @var UserRank $rank
//             */
//            $confines = json_decode($rank->confines, true);
//
//            $rank_set = false;
//            if (empty($confines)) {
//               var_dump('confs');
//                continue;
//            }
//
//
//
//
//            if ($licence->price < $confines['licence'])
//            {
//                var_dump('licence - '.$licence->price.':'.$confines['licence']);
//                continue;
//
//
//            }
//
//            foreach ($confines['ranks'] as $rank_id => $count) {
//                $my_refs = $this->myReferrals()->where('rank_id', '>=', $rank_id);
//                if ($rank_id == 1) {
//                    $my_refs = $my_refs->whereHas('deposits', function ($query) {
//                        $query->where('active', true);
//                    });
//                }
//                $refs_count = $my_refs->count();
//
//                var_dump($refs_count.':'.$count);
//
//                if ($refs_count >= $count)
//                {
//
//                    $rank_set = true;
//                }
//
//            }
//
//            if ($rank_set===true)
//            {
//                var_dump('setted rank '.$);
//            }
//
//
//
//        }
//    }


}
