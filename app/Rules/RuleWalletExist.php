<?php
namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\Auth;

/**
 * Class RuleWalletExist
 * @package App\Rules
 */
class RuleWalletExist implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {


        $wallet = user()->wallets()->where('currency_id', request('currency_id'))->where('payment_system_id', request('payment_system_id'))->first();

        if (empty($wallet)) {
            return false;
        }

        return null !== $wallet;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('validation.wrong_wallet');
    }
}
