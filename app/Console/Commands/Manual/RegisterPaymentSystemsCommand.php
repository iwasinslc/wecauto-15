<?php
namespace App\Console\Commands\Manual;

use App\Models\Currency;
use App\Models\PaymentSystem;
use Illuminate\Console\Command;

/**
 * Class RegisterPaymentSystemsCommand
 * @package App\Console\Commands\Manual
 */
class RegisterPaymentSystemsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'register:payment_systems {demo?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Register all needed payment systems.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $questions = [
            'perfectmoney' => [
                'answer' => $this->argument('demo') == true ? 'yes' : $this->ask('Perfect Money [yes|no]', 'no'),
                'name' => 'Perfect Money',
                'currencies' => [
                    'USD',
                    'EUR',
                ],
            ],

            'payeer' => [
                'answer' => $this->argument('demo') == true ? 'yes' : $this->ask('Payeer [yes|no]', 'no'),
                'name' => 'Payeer',
                'currencies' => [
                    'USD',
                    'EUR',
                    'RUR',
                ],
            ],

            'cryptocurencyapi' => [
                'answer' => $this->argument('demo') == true ? 'yes' : $this->ask('cryptocurencyapi [yes|no]', 'no'),
                'name' => 'cryptocurencyapi',
                'currencies' => [
                    'BTC',
                ],
            ],
            'etherapi' => [
                'answer' => $this->argument('demo') == true ? 'yes' : $this->ask('Ethereum [yes|no]', 'no'),
                'name' => 'Ethereum',
                'currencies' => [
                    'ETH',
                ],
            ],




            'webcoinapi' => [
                'answer' => $this->argument('demo') == true ? 'yes' : $this->ask('webcoinapi [yes|no]', 'no'),
                'name' => 'webcoinapi',
                'currencies' => [
                    'WEC',
                ],
            ],
            'custom' => [
                'answer' => $this->argument('demo') == true ? 'yes' : $this->ask('custom [yes|no]', 'no'),
                'name' => 'custom',
                'currencies' => [
                    'ACC',
                    'FST'
                ],
            ],

            'etherapiusdt' => [
                'answer' => $this->argument('demo') == true ? 'yes' : $this->ask('ERC20 [yes|no]', 'no'),
                'name' => 'ERC20',
                'currencies' => [
                    'USDT'
                ],
            ],



            'prizm' => [
                'answer' => $this->argument('demo') == true ? 'yes' : $this->ask('prizm [yes|no]', 'yes'),
                'name' => 'prizm',
                'currencies' => [
                    'PZM',
                ],
            ],

            'tronapiusdt' => [
                'answer' => $this->argument('demo') == true ? 'yes' : $this->ask('TRC20 [yes|no]', 'no'),
                'name' => 'TRC20',
                'currencies' => [
                    'USDT'
                ],
            ],
        ];

        foreach ($questions as $paymentSystemCode => $data) {
            $this->line('------');

            if ('yes' !== $data['answer']) {
                continue;
            }

            $this->info('Registering ' . $paymentSystemCode);
            $checkExists = PaymentSystem::where('code', $paymentSystemCode)->get()->count();

            if ($checkExists > 0) {
                $this->error($paymentSystemCode . ' already registered.');
                continue;
            }

            $reg = PaymentSystem::create([
                'name' => $data['name'],
                'code' => $paymentSystemCode
            ]);

            if (!$reg) {
                $this->error('Can not register ' . $paymentSystemCode);
                continue;
            }

            $this->info($paymentSystemCode . ' registered.');

            foreach ($data['currencies'] as $currency) {
                $currencyInfo = Currency::where('code', $currency)->first();

                if (empty($currencyInfo)) {
                    $this->warn('currency ' . $currency . ' is not registered, ' . $paymentSystemCode . ' will be without ' . $currency);
                    continue;
                }

                \DB::table('currency_payment_system')->insert([
                    'currency_id' => $currencyInfo->id,
                    'payment_system_id' => $reg->id,
                ]);

                $this->info('currency ' . $currency . ' registered for ' . $paymentSystemCode);
            }
        }
    }
}
