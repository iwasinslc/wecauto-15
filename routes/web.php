<?php

use Illuminate\Support\Facades\Route;

Route::post('/telegram_webhook/{token}', 'Telegram\TelegramWebhookController@index')->name('telegram.webhook');

Route::get('/telegram_topup/{user_id}/{wallet_id}/{payment_system_id}/{currency_id}/{rate_id}/{amount}', 'Telegram\TelegramTopupController@index')->name('telegram.topup');

Route::get('/play_partner/{partner_id}/{cyber_id}', 'PlayPartnerController@index')->name('play_partner');
Route::get('/partner/{partner_id}', 'SetPartnerController@index')->name('partner');
Route::group(['middleware' => ['web']], function () {
    Auth::routes();

    // oAuth
//    Route::get('login/callback/vk/{telegramUserId}', 'controller')->name('vk.redirect_url');

//    Route::get('login/redirect/google/{telegramUserId}', 'controller')->name('google.redirect_url');
//    Route::get('login/callback/google', 'controller');

    // social event catcher
    Route::get('youtube/watch/{taskAction}/{userId}', 'Technical\Youtube\YoutubeWatchCatchController@index')->name('youtube.watch');
    Route::any('youtube/watch/save/{taskAction}/{userId}', 'Technical\Youtube\YoutubeWatchCatchController@catchEvent')->name('youtube.watch.save');

    Route::post('/advcash/status', 'Payment\AdvcashController@status')->name('advcash.status');
    Route::post('/perfectmoney/status', 'Payment\PerfectMoneyController@status')->name('perfectmoney.status');
    Route::post('/payeer/status', 'Payment\PayeerController@status')->name('payeer.status');
    Route::post('/blockio/status', 'Payment\BlockioController@status')->name('blockio.status');
    Route::post('/coinpayments/status', 'Payment\CoinpaymentsController@status')->name('coinpayments.status');
    Route::post('/enpay/status', 'Payment\EnpayController@status')->name('enpay.status');
    Route::post('/nixmoney/status', 'Payment\NixmoneyController@status')->name('nixmoney.status');
    Route::post('/free-kassa/status', 'Payment\FreeKassaController@status')->name('free-kassa.status');
    Route::post('/yandex/status', 'Payment\FreeKassaController@status')->name('yandex.status');
    Route::post('/qiwi/status', 'Payment\FreeKassaController@status')->name('qiwi.status');
    Route::post('/visa_mastercard/status', 'Payment\FreeKassaController@status')->name('visa_mastercard.status');
    Route::post('/xpay/status', 'Payment\XpayController@status')->name('xpay.status');
    Route::any('/etherapi/status', 'Payment\EtherApiController@status')->name('etherapi.status');
    Route::any('/etherapiusdt/status', 'Payment\EtherApiUSDTController@status')->name('etherapiusdt.status');

    Route::any('/tronapiusdt/status', 'Payment\TronApiUSDTController@status')->name('tronapiusdt.status');

    Route::any('/webcoinapi/status', 'Payment\WebCoinApiController@status')->name('webcoinapi.status');
    Route::any('/cryptocurencyapi/status', 'Payment\CryptoCurrencyApiController@status')->name('cryptocurencyapi.status');
    Route::any('/bip/status', 'Payment\MinterApiController@status')->name('bip.status');
    Route::any('/prizm/status', 'Payment\PrizmApiController@status')->name('prizm.status');

    Route::post('/advcash/manual', 'Payment\AdvcashController@manual')->name('advcash.manual');

    Route::any('/custom/status', 'Payment\CustomApiController@status')->name('custom.status');

    Route::group(['middleware' => ['site.status']], function () {
        // Confirm email

        Route::get('/api/price', 'CodesController@price')->name('api.price');
        Route::get('/confirm_email/{hash}', 'Customer\ConfirmEmailController@index')->name('email.confirm');
        Route::get('/refereshcapcha', 'CodesController@refereshCapcha')->name('refereshcapcha');
        // Not authorized
        Route::get('/', 'Customer\MainController@index')->name('customer.main');
        Route::get('/about-us', 'Customer\AboutUsController@index')->name('customer.about-us');
        Route::get('/documents', 'Customer\DocumentsController@index')->name('customer.documents');
        Route::get('/investors', 'Customer\InvestorsController@index')->name('customer.investors');
        Route::get('/partners', 'Customer\PartnersController@index')->name('customer.partners');

        Route::get('/support', 'Customer\SupportController@index')->name('customer.support');
        Route::post('/support', 'Customer\SupportController@send')->name('customer.support');

        Route::get('/faq', 'Customer\FaqController@index')->name('customer.faq');
//        Route::get('/products', 'Customer\ProductsController@index')->name('customer.products');
        Route::get('/reviews', 'Customer\ReviewsController@index')->name('customer.reviews');
        Route::get('/agreement', 'Customer\AgreementController@index')->name('customer.agreement');
        Route::get('/privacy', 'Customer\AgreementController@privacy')->name('customer.privacy');
        Route::get('/queue', 'Customer\QueueController@index')->name('customer.queue');

        // Technical

        Route::get('/lang/{locale}', 'LanguageController@index')->name('set.lang');

        Route::get('/news/{slug}', 'Customer\NewsController@item')->name('customer.news.item');
        Route::get('/news', 'Customer\NewsController@index')->name('customer.news.index');


        // IPN

        // Transaction confirmation
        Route::get('/{type}/confirm/{confirmation_code}', 'Profile\TransferController@confirmTransaction')
            ->where([
                'type' => 'withdraw|transfer',
                'confirmation_code' => '[\w]{64,64}'
            ])
            ->name('transaction.confirm');
    });

    Route::group(['middleware' => ['auth', '2fa', 'auth_post_limit']], function () {
        Route::group(['middleware' => ['site.status']], function () {
            Route::get('/impersonate/leave', 'Admin\ImpersonateController@leave')->name('admin.impersonate.leave');


            Route::get('/profile/buy', 'Profile\BuyController@index')->name('profile.buy');
            Route::post('/profile/buy/store', 'Profile\BuyController@store')->name('profile.buy.store');

            Route::get('/profile/reftree', 'Technical\ReftreeController@show')->name('users.reftree');

            Route::get('/profile', 'Profile\ProfileController@index')->name('profile.profile');

            Route::get('/profile/statistic', 'Profile\StatisticController@index')->name('profile.statistic');

            Route::get('/profile/profile/charity', 'Profile\CharityController@index')->name('profile.charity');
            Route::post('/profile/charity/store', 'Profile\CharityController@store')->name('profile.charity.store');
            Route::get('/profile/operations', 'Profile\OperationsController@index')->name('profile.operations.index');
            Route::any('/profile/operations_dataTable/{type?}', 'Profile\OperationsController@datatable')->name('profile.operations.dataTable');
            Route::any('/profile/dataTable/withdraws', 'Profile\OperationsController@withdrawsDataTable')->name('profile.withdraws.dataTable');

            Route::get('/profile/affiliate_datatable/', 'Profile\AffiliateController@datatable')->name('profile.affiliate.dataTable');

            Route::get('/profile/affiliate/{lvl?}', 'Profile\AffiliateController@index')->name('profile.affiliate');
            Route::get('/profile/promo', 'Profile\PromoController@index')->name('profile.promo');

            // Payment authorization operation
//            Route::middleware(['payment_authorization'])->group(function() {
            Route::post('/transfer/store', 'Profile\TransferController@transfer')->name('profile.transfer.store');
            Route::post('/profile/withdraw', 'Profile\WithdrawController@handle')->name('profile.withdraw');
            Route::post('/profile/exchange/exchange', 'Profile\ExchangeController@exchange')->name('profile.exchange.exchange');
//            });

            Route::get('/profile/settings', 'Profile\SettingsController@index')->name('profile.settings');
            Route::post('/profile/settings', 'Profile\SettingsController@handle')->name('profile.settings');
            Route::post('/profile/settings/2fa', 'Profile\SettingsController@google2fa')->name('profile.settings_2fa');
            Route::get('/send-verification-email', 'Customer\ConfirmEmailController@send_verification_email')->name('profile.send_verification_email');

            Route::get('/profile/withdraw', 'Profile\WithdrawController@index')->name('profile.withdraw');

            Route::post('/profile/withdraw/reject/{id}', 'Profile\WithdrawController@reject')->name('profile.withdraw.reject');

//            Route::get('/profile/projects', 'Profile\ProjectsController@index')->name('profile.projects');

            // Cash back panel
            Route::prefix('/profile/cashback')->as('profile.cashback.')->group(function() {
                Route::get('/conditions', 'Profile\CashbackController@conditions')->name('conditions');
                Route::get('/create', 'Profile\CashbackController@create')->name('create');
                Route::get('/dataTable', 'Profile\CashbackController@dataTable')->name('dataTable');
                Route::post('/', 'Profile\CashbackController@store')->name('store');
                Route::get('/', 'Profile\CashbackController@index')->name('index');
                Route::get('/{id}', 'Profile\CashbackController@show')->name('show');
            });

            Route::prefix('/profile/cashback-result')->as('profile.cashback-result.')->middleware('role:root|moderator')->group(function() {
                Route::get('/', 'Profile\CashbackResultController@index')->name('index');
                Route::get('/datatable', 'Profile\CashbackResultController@datatable')->name('datatable');
                Route::put('/{id}', 'Profile\CashbackResultController@update')->name('update');
                Route::get('/{id}', 'Profile\CashbackResultController@show')->name('show');
            });

            Route::get('/profile/topup', 'Profile\TopupController@index')->name('profile.topup');
            Route::post('/profile/topup', 'Profile\TopupController@handle')->name('profile.topup');


            Route::get('/topup/perfectmoney', 'Payment\PerfectMoneyController@topUp')->name('profile.topup.perfectmoney');
            Route::get('/topup/payeer', 'Payment\PayeerController@topUp')->name('profile.topup.payeer');

            Route::get('/profile/topup/webcoinapi', 'Payment\WebCoinApiController@topUp')->name('profile.topup.webcoinapi');
            Route::get('/topup/etherapi', 'Payment\EtherApiController@topUp')->name('profile.topup.etherapi');
            Route::get('/topup/etherapiusdt', 'Payment\EtherApiUSDTController@topUp')->name('profile.topup.etherapiusdt');

            Route::get('/topup/tronapiusdt', 'Payment\TronApiUSDTController@topUp')->name('profile.topup.tronapiusdt');

            Route::get('/topup/cryptocurencyapi', 'Payment\CryptoCurrencyApiController@topUp')->name('profile.topup.cryptocurencyapi');
//            Route::get('/topup/bip', 'Payment\MinterApiController@topUp')->name('profile.topup.bip');
            Route::get('/topup/prizm', 'Payment\PrizmApiController@topUp')->name('profile.topup.prizm');
            Route::get('/profile/topup/custom', 'Payment\CustomApiController@topUp')->name('profile.topup.custom');

            Route::any('/profile/topup/payment_message', 'Profile\TopupController@paymentMessage')->name('profile.topup.payment_message');


            Route::get('/profile/exchange', 'Profile\ExchangeController@index')->name('profile.exchange');

            Route::get('/profile/exchange/wec', 'Profile\ExchangeController@wec_exchange')->name('profile.exchange_wec');
            Route::get('/profile/exchange/currency/{mainCode}-{code}', 'Profile\ExchangeController@currency')->name('profile.exchange.currency');

            Route::get('/profile/exchange/sell_orders/{mainCode}-{code}', 'Profile\ExchangeController@sell_orders')->name('profile.exchange.sell_orders');
            Route::get('/profile/exchange/buy_orders/{mainCode}-{code}', 'Profile\ExchangeController@buy_orders')->name('profile.exchange.buy_orders');
            Route::get('/profile/exchange/my_orders/{mainCode}-{code}', 'Profile\ExchangeController@my_orders')->name('profile.exchange.my_orders');
            Route::get('/profile/exchange/dataTableExchange/{mainCode}-{code}', 'Profile\ExchangeController@dataTableExchange')->name('profile.exchange.dataTableExchange');
            Route::get('/profile/exchange/dataTableExchangeAll/{mainCode}-{code}', 'Profile\ExchangeController@dataTableExchangeAll')->name('profile.exchange.dataTableExchangeAll');

            Route::post('/profile/exchange/close-order/{id}', 'Profile\ExchangeController@close_order')->name('profile.exchange.close-order');

            Route::get('/transfer', 'Profile\TransferController@index')->name('profile.transfer');

            Route::get('/profile/licences', 'Profile\LicenceController@index')->name('profile.licence');
            Route::post('/profile/licences/buy', 'Profile\LicenceController@buy_licence')->name('profile.buy_licence');

            Route::get('/profile/systems', 'Profile\ProfileController@systems')->name('profile.systems');


            Route::get('/profile/delete-notification/{id}', 'Profile\ProfileController@delete_notification')->name('profile.delete-notification');
            Route::get('/profile/check_amount', 'Profile\ProfileController@check_amount')->name('profile.check_amount');


            Route::get('/profile/reviews', 'Profile\ReviewsController@index')->name('profile.reviews');
            Route::get('/profile/reviews/add', 'Profile\ReviewsController@add')->name('profile.reviews.add');
            Route::post('/profile/reviews/store', 'Profile\ReviewsController@store')->name('profile.reviews.store');

            Route::post('/image-crop', 'Profile\ImageController@imageCropPost');
            Route::get('/remove-image', 'Profile\ImageController@remove')->name('remove_image');


            Route::get('/profile/deposits', 'Profile\DepositsController@index')->name('profile.deposits');

            Route::group(['middleware' => ['deposit_licence']], function () {

                Route::get('/profile/deposits/create', 'Profile\DepositsController@create')->name('profile.deposits.create');
                Route::post('/profile/deposits/create_handle', 'Profile\DepositsController@create_handle')->name('profile.deposits.create_handle');
                Route::get('/profile/deposits/add_photo', 'Profile\DepositsController@add_photo')->name('profile.deposits.add_photo');
                Route::post('/profile/deposits/add_photo_handle', 'Profile\DepositsController@add_photo_handle')->name('profile.deposits.add_photo_handle');
                Route::get('/profile/deposits/add_amount', 'Profile\DepositsController@add_amount')->name('profile.deposits.add_amount');
                Route::post('/profile/deposits/add_amount_handle', 'Profile\DepositsController@add_amount_handle')->name('profile.deposits.add_amount_handle');
                Route::get('/profile/deposits/finish', 'Profile\DepositsController@finish')->name('profile.deposits.finish');
                Route::post('/profile/deposits/finish_handle', 'Profile\DepositsController@finish_handle')->name('profile.deposits.finish_handle');
            });
            Route::get('/profile/deposits/add_fst/{id}', 'Profile\DepositsController@add_fst')->name('profile.deposits.add_fst');
            Route::get('/profile/deposits/change_days/{id}', 'Profile\DepositsController@change_days')->name('profile.deposits.change_days');
            Route::post('/profile/deposits/add_fst_handle', 'Profile\DepositsController@add_fst_handle')->name('profile.deposits.add_fst_handle');

        });
        Route::domain(env('SUBD'))->group(function () {
            Route::prefix('admin')->namespace('Admin')->group(function () {
                // Controllers Within The "App\Http\Controllers\Admin" Namespace
                Route::group(['middleware' => ['role:root|admin|writer']], function () {
                    Route::get('/', 'DashboardController@index')->name('admin');


                    Route::resource('/translations', 'TplTranslationsController', ['names' => [
                        'index' => 'admin.tpl_texts.index',
                        'index/{category?}' => 'admin.tpl_texts.index',
                        'create' => 'admin.tpl_texts.create',
                        'store' => 'admin.tpl_texts.store',
                        'edit' => 'admin.tpl_texts.edit',
                        'update' => 'admin.tpl_texts.update',
                        'destroy' => 'admin.tpl_texts.destroy',
                    ]]);


                    Route::resource('/news', 'NewsController', ['names' => [
                        'index' => 'admin.news.index',
                        'create' => 'admin.news.create',
                        'store' => 'admin.news.store',
                        'edit' => 'admin.news.edit',
                        'update' => 'admin.news.update',
                        'destroy' => 'admin.news.destroy',
                    ]]);

                    Route::resource('/reviews', 'ReviewsController', ['names' => [
                        'index' => 'admin.reviews.index',
                        'create' => 'admin.reviews.create',
                        'store' => 'admin.reviews.store',
                        'edit' => 'admin.reviews.edit',
                        'update' => 'admin.reviews.update',
                        'destroy' => 'admin.reviews.destroy',
                    ]]);
                    Route::resource('/faqs', 'FaqsController', ['names' => [
                        'index' => 'admin.faqs.index',
                        'create' => 'admin.faqs.create',
                        'store' => 'admin.faqs.store',
                        'edit' => 'admin.faqs.edit',
                        'update' => 'admin.faqs.update',
                        'destroy' => 'admin.faqs.destroy',
                    ]]);


                });


                Route::group(['middleware' => ['role:root|admin']], function () {

                    Route::prefix('/cashback')->as('admin.cashback.')->group(function() {
                        Route::get('dataTable', 'CashbackRequestController@dataTable')->name('dataTable');
                        Route::get('/export', 'CashbackRequestController@export')->name('export');
                        Route::resource('/', 'CashbackRequestController', ['parameters' => [
                            '' => 'cashback'
                        ]])->only(['index', 'show', 'update']);
                    });

                    Route::get('/impersonate/{id}', 'ImpersonateController@impersonate')->name('admin.impersonate');

                    Route::get('/deposits/block/{deposit}', 'DepositController@block')->name('admin.deposits.block');
                    Route::post('/deposits/change_rate/{deposit}', 'DepositController@changeRate')->name('admin.deposits.update_rate');
                    Route::post('/deposits/delete/{deposit}', 'DepositController@delete')->name('admin.deposits.delete');
                    Route::get('/deposits/unblock/{deposit}', 'DepositController@unblock')->name('admin.deposits.unblock');
                    Route::get('/deposits/dtdata', 'DepositController@dataTable')->name('admin.deposits.dtdata');
                    Route::resource('/deposits', 'DepositController', ['names' => [
                        'index' => 'admin.deposits.index',
                        'show' => 'admin.deposits.show',
                    ]]);
                    Route::post('/deposits', 'DepositController@filter')->name('admin.deposits.filter');


                    Route::get('/wallets/dtdata', 'WalletsController@dataTable')->name('admin.wallets.dtdata');
                    Route::get('/wallets/', 'WalletsController@index')->name('admin.wallets');


                    Route::get('/transactions/dtdata', 'TransactionsController@dataTable')->name('admin.transactions.dtdata');
                    Route::post('/transactions/approve-many', 'TransactionsController@approveMany')->name('admin.transactions.approve-many');

                    Route::get('/transactions/delete/{id}', 'TransactionsController@delete')->name('admin.transactions.delete');
                    Route::get('/transactions/approve/{id}', 'TransactionsController@approve')->name('admin.transactions.approve');
                    Route::resource('/transactions', 'TransactionsController', ['names' => [
                        'index' => 'admin.transactions.index',
                        'show' => 'admin.transactions.show',
                    ]]);

                    Route::resource('/langs', 'LanguagesController', ['names' => [
                        'index' => 'admin.langs.index',
                        'create' => 'admin.langs.create',
                        'store' => 'admin.langs.store',
                        'edit' => 'admin.langs.edit',
                        'update' => 'admin.langs.update',
                    ]]);
                    Route::get('/langs/destroy/{id}', 'LanguagesController@destroy')->name('admin.langs.destroy');

                    Route::get('/users/reftree/{id}', 'Technical\ReftreeController@show')->name('admin.users.reftree');
                    Route::get('/users/dtdata', 'UsersController@dataTable')->name('admin.users.dtdata');
                    Route::get('/users/dt-transactions/{user_id}', 'UsersController@dataTableTransactions')->name('admin.users.dt-transactions');
                    Route::get('/users/dt-deposits/{user_id}', 'UsersController@dataTableDeposits')->name('admin.users.dt-deposits');
                    Route::get('/users/dt-wrs/{user_id}', 'UsersController@dataTableWrs')->name('admin.users.dt-wrs');
                    Route::get('/users/dt-pvs/{user_id}', 'UsersController@dataTablePageViews')->name('admin.users.dt-pvs');
                    Route::get('/users/dt-trades/{user_id}', 'UsersController@dataTableTrades')->name('admin.users.dt-trades');
                    Route::get('/users/dt-orders/{user_id}', 'UsersController@dataTableOrders')->name('admin.users.dt-orders');
                    Route::resource('/users', 'UsersController', ['names' => [
                        'index' => 'admin.users.index',
                        'show' => 'admin.users.show',
                        'show/{level?}{plevel?}' => 'admin.users.show.level',
                        'edit' => 'admin.users.edit',
                        'update' => 'admin.users.update',
                        'destroy' => 'admin.users.destroy',
                    ]]);
                    Route::group(['middleware' => ['role:root']], function () {
                        Route::post('/users/bonus', 'UsersController@bonus')->name('admin.users.bonus');
                        Route::post('/users/penalty', 'UsersController@penalty')->name('admin.users.penalty');
                    });
                    Route::post('/users', 'UsersController@filter')->name('admin.users.filter');

                    Route::get('/users/reset2fa/{id}', 'UsersController@reset2fa')->name('admin.users.reset2fa');


                    Route::get('/export_ranks', 'UsersController@export_ranks')->name('admin.users.export_ranks');
                });

                Route::group(['middleware' => ['role:root']], function () {


                    Route::get('/statistic', 'StatisticController@index')->name('admin.statistic');

                    Route::get('/promo/usd', 'PromoController@usd')->name('admin.promo.usd');
                    Route::get('/promo/acc', 'PromoController@acc')->name('admin.promo.acc');

                    Route::get('/promo/ddata_usd', 'PromoController@ddata_usd')->name('admin.promo.ddata_usd');
                    Route::get('/promo/ddata_acc', 'PromoController@ddata_acc')->name('admin.promo.ddata_acc');


                    Route::get('/email_marketing', 'EmailMarketingController@index')->name('admin.email_marketing');
                    Route::post('/email_marketing', 'EmailMarketingController@handle')->name('admin.email_marketing.handle');

                    Route::get('/settings', 'SettingsController@index')->name('admin.settings.index');
                    Route::get('/settings/switch_site_status', 'SettingsController@switchSiteStatus')->name('admin.settings.switchSiteStatus');
                    Route::post('/settings/change-many', 'SettingsController@changeMany')->name('admin.settings.change-many');


                    Route::get('/requests/approve/{id}', 'WithdrawalRequestsController@approve')->name('admin.requests.approve');
                    Route::post('/requests/approve-many', 'WithdrawalRequestsController@approveMany')->name('admin.requests.approve-many');
                    Route::get('/requests/reject/{id}', 'WithdrawalRequestsController@reject')->name('admin.requests.reject');
                    Route::get('/requests/approveManually/{id}', 'WithdrawalRequestsController@approveManually')->name('admin.requests.approveManually');
                    Route::get('/requests/dtdata', 'WithdrawalRequestsController@dataTable')->name('admin.requests.dtdata');
                    Route::get('/requests/all', 'WithdrawalRequestsController@all')->name('admin.requests.all');
                    Route::get('/requests/dtdataall', 'WithdrawalRequestsController@dataTableAll')->name('admin.requests.dtdataall');
                    Route::resource('/requests', 'WithdrawalRequestsController', ['names' => [
                        'index' => 'admin.requests.index',
                        'show' => 'admin.requests.show',
                        'edit' => 'admin.requests.edit',
                        'update' => 'admin.requests.update',
                    ]]);


                    Route::resource('/currencies', 'CurrenciesController', ['names' => [
                        'index' => 'admin.currencies.index',
                        'edit' => 'admin.currencies.edit',
                        'update' => 'admin.currencies.update',
                    ]]);
                    Route::resource('/payment-systems', 'PaymentSystemsController', ['names' => [
                        'index' => 'admin.payment-systems.index',
                        'edit' => 'admin.payment-systems.edit',
                        'update' => 'admin.payment-systems.update',
                    ]]);

                    Route::resource('/referral', 'ReferralController', ['names' => [
                        'index' => 'admin.referral.index',
                        'create' => 'admin.referral.create',
                        'store' => 'admin.referral.store',
                        'edit' => 'admin.referral.edit',
                        'update' => 'admin.referral.update',
                    ]]);
                    Route::get('/referral/destroy/{id}', 'ReferralController@destroy')->name('admin.referral.destroy');

                    Route::resource('/rates', 'RateController', ['names' => [
                        'index' => 'admin.rates.index',
                        'show' => 'admin.rates.show',
                        'create' => 'admin.rates.create',
                        'store' => 'admin.rates.store',
                        'edit' => 'admin.rates.edit',
                        'update' => 'admin.rates.update',
                    ]]);
                    Route::get('/rates/destroy/{id}', 'RateController@destroy')->name('admin.rates.destroy');
                    Route::get('/licences', 'LicencesController@index')->name('admin.licences.index');
                    Route::get('/licences/statistic', 'LicencesController@statistic')->name('admin.licences.statistic');
                    Route::get('/licences/{id}/edit', 'LicencesController@edit')->name('admin.licences.edit');
                    Route::any('/licences/{id}/update', 'LicencesController@update')->name('admin.licences.update');
                    Route::any('/licences/{id}/destroy', 'LicencesController@destroy')->name('admin.licences.destroy');
                    Route::get('/licences/create', 'LicencesController@create')->name('admin.licences.create');
                    Route::any('/licences/store', 'LicencesController@store')->name('admin.licences.store');


                    Route::get('/projects', 'ProjectsController@index')->name('admin.projects.index');
                    Route::get('/projects/{id}/edit', 'ProjectsController@edit')->name('admin.projects.edit');
                    Route::any('/projects/{id}/update', 'ProjectsController@update')->name('admin.projects.update');
                    Route::any('/projects/{id}/destroy', 'ProjectsController@destroy')->name('admin.projects.destroy');
                    Route::get('/projects/create', 'ProjectsController@create')->name('admin.projects.create');
                    Route::any('/projects/store', 'ProjectsController@store')->name('admin.projects.store');


                    Route::get('/social_meta', 'SocialMetaController@index')->name('admin.social_meta.index');
                    Route::get('/social_meta/dtdata', 'SocialMetaController@dataTable')->name('admin.social_meta.dtdata');

                    /*
                     * Telegram
                     */
                    Route::resource('/telegram/bots', 'Telegram\BotsController', ['names' => [
                        'index' => 'admin.telegram.bots.index',
                        'create' => 'admin.telegram.bots.create',
                        'store' => 'admin.telegram.bots.store',
                        'edit' => 'admin.telegram.bots.edit',
                        'update' => 'admin.telegram.bots.update',
                    ]]);
                    Route::get('/telegram/datatable/bots/{id}/destroy', 'Telegram\BotsController@destroy')->name('admin.telegram.bots.destroy');
                    Route::get('/telegram/datatable/bots', 'Telegram\BotsController@datatable')->name('admin.telegram.bots.datatable');

                    Route::get('/telegram/events', 'Telegram\EventsController@index')->name('admin.telegram.events.list');
                    Route::get('/telegram/datatable/events', 'Telegram\EventsController@datatable')->name('admin.telegram.events.datatable');

                    Route::get('/telegram/messages', 'Telegram\MessagesController@index')->name('admin.telegram.messages.list');
                    Route::get('/telegram/datatable/messages', 'Telegram\MessagesController@datatable')->name('admin.telegram.messages.datatable');

                    Route::get('/telegram/users', 'Telegram\UsersController@index')->name('admin.telegram.users.list');
                    Route::get('/telegram/datatable/users', 'Telegram\UsersController@datatable')->name('admin.telegram.users.datatable');

                    Route::get('/telegram/webhooks', 'Telegram\WebhooksController@index')->name('admin.telegram.webhooks.list');
                    Route::get('/telegram/datatable/webhooks', 'Telegram\WebhooksController@datatable')->name('admin.telegram.webhooks.datatable');

                    Route::get('/telegram/webhooks_info', 'Telegram\WebhooksInfoController@index')->name('admin.telegram.webhooks_info.list');
                    Route::get('/telegram/datatable/webhooks_info', 'Telegram\WebhooksInfoController@datatable')->name('admin.telegram.webhooks_info.datatable');

                    /*
                     * User tasks
                     */
                    Route::resource('/user-tasks/tasks', 'UserTasks\TasksController', ['names' => [
                        'index' => 'admin.user-tasks.tasks.index',
                        'create' => 'admin.user-tasks.tasks.create',
                        'store' => 'admin.user-tasks.tasks.store',
                        'edit' => 'admin.user-tasks.tasks.edit',
                        'update' => 'admin.user-tasks.tasks.update',
                    ]]);
                    Route::get('/user-tasks/datatable/tasks/{id}/destroy', 'UserTasks\TasksController@destroy')->name('admin.user-tasks.tasks.destroy');
                    Route::get('/user-tasks/datatable/tasks', 'UserTasks\TasksController@datatable')->name('admin.user-tasks.tasks.datatable');

                    Route::get('/user-tasks/accepted_tasks', 'UserTasks\AcceptedTasksController@index')->name('admin.user-tasks.accepted_tasks.list');
                    Route::get('/user-tasks/datatable/accepted_tasks', 'UserTasks\AcceptedTasksController@datatable')->name('admin.user-tasks.accepted_tasks.datatable');

                    Route::get('/user-tasks/available_elements', 'UserTasks\AvailableElementsController@index')->name('admin.user-tasks.available_elements.list');
                    Route::get('/user-tasks/datatable/available_elements', 'UserTasks\AvailableElementsController@datatable')->name('admin.user-tasks.available_elements.datatable');

                    Route::get('/user-tasks/tasks_elements', 'UserTasks\TasksElementsController@index')->name('admin.user-tasks.tasks_elements.list');
                    Route::get('/user-tasks/datatable/tasks_elements', 'UserTasks\TasksElementsController@datatable')->name('admin.user-tasks.tasks_elements.datatable');

                    Route::get('/user-tasks/user_task_elements', 'UserTasks\UserTaskElementsController@index')->name('admin.user-tasks.user_task_elements.list');
                    Route::get('/user-tasks/datatable/user_task_elements', 'UserTasks\UserTaskElementsController@datatable')->name('admin.user-tasks.user_task_elements.datatable');

                    Route::get('/page_views/{id}', 'PageViewsController@show')->name('admin.page_views.show');

                    Route::get('/backup', 'BackupController@index')->name('admin.backup.index');
                    Route::get('/backup/backupDB', 'BackupController@backupDB')->name('admin.backup.backupDB');
                    Route::get('/backup/backupFiles', 'BackupController@backupFiles')->name('admin.backup.backupFiles');
                    Route::get('/backup/backupAll', 'BackupController@backupAll')->name('admin.backup.backupAll');
                    Route::get('/backup/destroy/{file}', 'BackupController@destroy')->where('file', '(.*(?:%2F:)?.*)')->name('admin.backup.destroy');
                    Route::post('/backup/download', 'BackupController@download')->name('admin.backup.download');

                    Route::get('/failedjobs', 'FailedJobsController@index')->name('admin.failedjobs.index');
                    Route::get('/failedjobs/datatable', 'FailedJobsController@dataTable')->name('admin.failedjobs.datatable');

                    Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index')->name('logs');
                    Route::get('see_integration_example/{functionName}', 'SeeIntegrationExample@index')->name('integration-docs');

                    Route::get('/sys_load', 'SysLoadController@index')->name('admin.sys_load');
                });
            });
        });

    });
});
