<?php
return [
    'emails' => [
        'spam_protection'   => 'Die E-Mail ist nicht gesendet. Spam-Regel.',
        'sent_successfully' => 'Die E-Mail ist erfolgreich gesendet',
        'unable_to_send'    => 'Diese E-Mail kann nicht gesendet werden',

        'request' => [
            'email_required' => 'Email ist ein Pflichtfeld.',
            'email_max'      => 'Maximale E-Mail-Länge beträgt 255 Zeichen',
            'email_email'    => 'Ungültige E-Mail',

            'text_required' => 'Text ist ein Pflichtfeld.',
            'text_min'      => 'Minimale Textlänge beträgt 10 Zeichen',
        ],
    ],
    'transaction_types' => [
        'enter'      => 'Bilanzauffüllung',
        'withdraw'   => 'Geldmittelabzug',
        'bonus'      => 'Bonus',
        'partner'    => 'Partnerprovision',
        'dividend'   => 'Einkommen aus Depot',
        'create_dep' => 'Depot eröffnen',
        'close_dep'  => 'Depot schließen',
        'penalty'    => 'Strafe',
    ],

];
